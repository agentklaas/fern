#pragma once
#include "utils.h"
#include <cmath>

namespace Fern
{
    namespace Kernels
    {

        //At the moment, the kernels take the template paremeter D but the functions are still hard-coded for 3 dimensions.
        template<typename T, size_t D>
        struct Gaussian
        {
            T A;
            T B;
            T C;
            T cutoff;

            constexpr Gaussian(const Grid<T,D>& grid, const T& width): A(1.0/std::pow(width*Utils::sqrt2pi<T>, D)), B(-0.5/std::pow(width,2)), C(2.0*A*B), cutoff(3.0*width)
            {

            }

            constexpr T operator()(const Tensor<T,D>& r) const
            {
                return A * exp(r.normsq()*B);
            }

            constexpr Tensor<T,D> gradient(const Tensor<T,D>& r) const
            {
                return (C * exp(r.normsq()*B)) * r;
            }
        };

        template<typename T, size_t D>
        struct Heaviside
        {
            T A;
            T B;
            T cutoff;

            constexpr Heaviside(const Grid<T,D>& grid, const T& width): A(1.0/(Utils::sphvol<T>*std::pow(width,3))), B(std::pow(width,2)), cutoff(width)
            {

            }

            constexpr T operator()(const Tensor<T,D>& r) const
            {
                return (r.normsq()<B) ? A : 0.0;
            }
        };


    }
}
