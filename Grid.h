#pragma once

#include "Tensor.h"
#include "Box.h"


namespace Fern
{

    template<typename T, size_t D>
    struct Grid
    {
        //fundamental properties
        Tensor<size_t,D> cells;
        Tensor<T,D> min, max;

        //accelerators
        Tensor<T,D> cells_floating;
        Tensor<T,D> reciprocal_size;
        Tensor<T,D> cell_size;
        Tensor<T,D> cells_per_size;
        Tensor<T,D> reciprocal_cell_size;
        Tensor<T,D> reciprocal_double_cell_size;
        T cell_volume;
        Tensor<size_t,D> strides;


        constexpr Grid();
        constexpr Grid(Tensor<T,D> inMin, Tensor<T,D> inMax, Tensor<size_t,D> inCells);

        constexpr size_t index(const Tensor<size_t,D>& indices) const;
        constexpr size_t size() const;
        constexpr Tensor<T,D> cast_cells(const Tensor<size_t,D>& inCells) const;
        constexpr Tensor<size_t,D> cell_at(const Tensor<T,D>& pos) const;
        constexpr Tensor<size_t,D> bounded_cell_at(const Tensor<T,D>& pos) const;
        constexpr Tensor<T,D> cell_lower_bound(const Tensor<size_t,D>& in) const;
        constexpr Tensor<T,D> cell_upper_bound(const Tensor<size_t,D>& in) const;
        constexpr Tensor<T,D> cell_center(const Tensor<size_t,D>& in) const;
        constexpr Box<T,D> cell_box(const Tensor<size_t,D>& in) const;

        template<size_t N=0> constexpr bool contains(const Tensor<T,D>& in) const;
    };

    

    template<typename T, size_t D>
    constexpr Grid<T,D>::Grid()
    {

    }

    template<typename T, size_t D>
    constexpr Grid<T,D>::Grid(Tensor<T,D> inMin, Tensor<T,D> inMax, Tensor<size_t,D> inCells): min(inMin), max(inMax), cells(inCells), cells_floating(cast_cells(cells)), reciprocal_size(T(1.0)/(max-min)), cell_size((max-min)/cells_floating), reciprocal_cell_size(T(1.0)/cell_size), reciprocal_double_cell_size(T(0.5)/cell_size), cell_volume(cell_size.product()), cells_per_size(cells_floating*reciprocal_size)
    {
        for(size_t i=0; i<D; i++)
        {
            strides[i] = 1;
            for(size_t j=i+1; j<D; j++)
            {
                strides[i] *= cells[j];
            }
        }
    }

    template<typename T, size_t D>
    constexpr size_t Grid<T,D>::index(const Tensor<size_t,D>& indices) const
    {
        size_t out = 0;
        for(size_t i=0; i<D; i++)
        {
            out += indices[i]*strides[i];
        }
        return out;
    }

    template<typename T, size_t D>
    constexpr size_t Grid<T,D>::size() const
    {
        return cells.product();
    }



    template<typename T, size_t D>
    constexpr Tensor<T,D> Grid<T,D>::cast_cells(const Tensor<size_t,D>& inCells) const
    {
        Tensor<T,D> out;
        for(size_t i=0; i<D; i++)
        {
            out[i] = (T)inCells[i];
        }
        return out;
    }

    template<typename T, size_t D>
    constexpr Tensor<size_t,D> Grid<T,D>::cell_at(const Tensor<T,D>& pos) const
    {
        Tensor<size_t,D> out;
        for(size_t i=0; i<D; i++)
        {
            out[i] = (size_t)( (pos[i]-min[i]) * cells_per_size[i] );
        }
        return out;
    }

    template<typename T, size_t D>
    constexpr Tensor<size_t,D> Grid<T,D>::bounded_cell_at(const Tensor<T,D>& pos) const
    {
        Tensor<size_t,D> out = cell_at(pos);
        for(int i=0; i<D; i++)
        {
            if(out[i] < 0)
            {
                out[i] = 0;
            }
            else if(out[i] >= cells[i])
            {
                out[i] = cells[i]-1;
            }
        }
        return out;

        //Tensor<size_t,D> out;
        //for(size_t i=0; i<D; i++)
        //{
        //    size_t tmp = (size_t)( (pos[i]-min[i]) * cells_per_size[i] );
        //    out[i] = tmp>0 ? (tmp<cells[i] ? tmp : cells[i]-1) : 0;
        //}
        //return out;
    }

    template<typename T, size_t D>
    constexpr Tensor<T,D> Grid<T,D>::cell_lower_bound(const Tensor<size_t,D>& in) const
    {
        Tensor<T,D> out;
        for(size_t i=0; i<D; i++)
        {
            out[i] = min[i] + (T)in[i] * cell_size[i];
        }
        return out;
    }

    template<typename T, size_t D>
    constexpr Tensor<T,D> Grid<T,D>::cell_upper_bound(const Tensor<size_t,D>& in) const
    {
        Tensor<T,D> out;
        for(size_t i=0; i<D; i++)
        {
            out[i] = min[i] + (T)(in[i]+1) * cell_size[i];
        }
        return out;
    }

    template<typename T, size_t D>
    constexpr Tensor<T,D> Grid<T,D>::cell_center(const Tensor<size_t,D>& in) const
    {
        Tensor<T,D> out;
        for(size_t i=0; i<D; i++)
        {
            out[i] = min[i] + ((T)in[i] + 0.5) * cell_size[i];
        }
        return out;
    }

    template<typename T, size_t D>
    constexpr Box<T,D> Grid<T,D>::cell_box(const Tensor<size_t,D>& in) const
    {
        return Box<T,D>{cell_lower_bound(in), cell_upper_bound(in)};
    }

    template<typename T, size_t D> template<size_t N>
    constexpr bool Grid<T,D>::contains(const Tensor<T,D>& in) const
    {
        if constexpr(N+1 < D)
        {
            return contains<N+1>(in) && in[N] >= min[N] && in[N] <= max[N];
        }
        else
        {
            return in[N] >= min[N] && in[N] <= max[N];
        }
    }



}
