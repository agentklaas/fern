Fern
===

Features
---
A reasonably efficient, header-only library to calculate fields from discrete elements.
It offers coarse graining, useful for some local physics calculations, and a more classical discretization approach, that promises to conserve the total quantities.
The classical approach is usually much faster.

To calculate a gradient in the classical discretization approach, one would simply compute the difference between neighbouring cells and divide by the distance.
That is not the case in coarse graining.
Thus, this library also offers a method to compute the coarse grained gradient fields.

Also, this library is almost ready to work in any number of dimensions.  
Ever wanted to know the coarse grained fields of your 23-dimensional particles?  
Then this library is for you!





Dependencies
---
At the very least you will need a c++17 capable compiler.
Additionally, I recommend cmake for building.

Apart from these programs, this library depends on two other projects as git submodules.
They are libraries, which I originally developed for just this project, but which have transcended that use since.  
**Stine** defines the Tensor struct, which is widely used throghout the entirety of this project. It has been optimized quite a bit.  
**Comet** defines the Tuple struct, which is used as an alternative to the std::tuple class. It has certain advantages, which you can read up on in its README file.



How to Build
---
If you just cloned this project and want to build it for the first time, you will not have the submodules.
To fetch them type:

```
git submodule update --init --recursive
```


Or if you already have an outdated version of the submodules and want to update them:

```
git submodule update --remote --recursive
```


Once you have the submodules, you can build the project very easily by using cmake.
I recommend building in a subdirectory.

```
mkdir build
cd build
cmake ..
make
```



Files
===

Grid.h
---
This defines the Grid struct, which holds the bounds and number of cells for the discretization.
It also pre-computes a number of quantities, used to accelerate the actual discretization process.


Box.h
---
The Box struct holds the minimum and maximum bounds of an axis aligned bounding box.
This file also defines a function to compute the intersection volume of two such boxes which are known to intersect.


Kernels.h
---
In this file you will find the definitions of the coarse graining kernels.
Each kernel is a struct that also pre-computes a number of quantities to accelerate the discretization process.


utils.h
---
This file contains some magic functions that operate on vectors of tuples and stuff, as well as some magic type aliases.
(Template parameters can not easily be looped through at this point in time, so one has to apply some tricks.)  
Apart from the magic, there are some constants defined in here.


Discretization.h
---
This file contains the core functionality of this library.
Namely the Discretize(...), CG<...>(...) and CGGradient<...>(...) functions.


main.cpp
---
The only compilation unit in this project.
It shows an examplary use of the library, along with a couple of annotations.


LICENSE
---
The project is published under the very permissive MIT license, which you will find in this file.


README.md
---
This file.
