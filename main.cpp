#include <iostream>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <random>
#include "Discretization.h"
#include "Kernels.h"

std::random_device rd;  //obtain a random number from hardware
std::mt19937 eng(rd()); //seed the pseudo random number generator



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//The discretization functions take a vector of a struct with certain members as a parameter.
//The names of the required members are fixed. Omitting them should lead to a compile error.
//The type of position and radius is fixed, but the tuple that makes up properties is variable.
//The components of the Tuple all have to be Tensors, though.
//It uses the Tuple struct from the Comet submodule because it has certain advantages over the std::tuple class.
//Likewise, within this library, Tensors from the Stine submodule are ubiquitous.
//Unfortunately, with the current state of most C++ compilers, we cannot make use of named tuples.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, size_t D>
struct Particle
{
    Tensor<T,D> position;
    T radius;
    Tuple<Tensor<T>, Tensor<T,D>> properties;
};



using namespace Fern;
using T = double;

int main()
{
    //define the discretization region
    Tensor<T,3> min{0.0,0.0,0.0};
    Tensor<T,3> max{1.0,1.0,1.0};
    Tensor<size_t,3> cells{100, 100, 100};
    Grid grid = Grid(min, max, cells);


    //initialize the random number distributions
    std::uniform_real_distribution<T> randPos{0.05,0.95};
    std::uniform_real_distribution<T> randRad{0.001,0.01};
    std::uniform_real_distribution<T> randMass{0.01,1.00};
    std::uniform_real_distribution<T> randVel{-0.1,0.1};


    //generate random particles (without any collision checks etc.)
    std::vector<Particle<T,3>> particles;
    for(size_t i=0; i<100000; i++)
    {
        Particle<T,3> p;
        p.position = {randPos(eng), randPos(eng), randPos(eng)};
        p.radius = randRad(eng);
        p.properties.get<0>() = {randMass(eng)};
        p.properties.get<1>() = {randVel(eng), randVel(eng), randVel(eng)};
        particles.push_back(p);
    }


    //print property totals
    Tensor<T> total_particle_mass{0.0};
    Tensor<T,3> total_particle_velocity{0.0,0.0,0.0};
    for(size_t i=0; i<particles.size(); i++)
    {
        total_particle_mass += particles[i].properties.get<0>();
        total_particle_velocity += particles[i].properties.get<1>();
    }
    std::cout<<"Particle Totals:\t\tmass="<<total_particle_mass<<"\tvelocity="<<total_particle_velocity<<"\n";



    //Discretization
    auto dscr_field = Discretize(particles, grid);
    Tensor<T> total_dscr_mass{0.0};
    Tensor<T,3> total_dscr_velocity{0.0,0.0,0.0};
    for(size_t i=0; i<grid.size(); i++)
    {
        total_dscr_mass += dscr_field[i].get<0>();   
        total_dscr_velocity += dscr_field[i].get<1>();   
    }
    std::cout<<"Discretization Totals:\t\tmass="<<total_dscr_mass*grid.cell_volume<<"\tvelocity="<<total_dscr_velocity*grid.cell_volume<<"\n";


    //Gaussian Coarse Graining
    auto gauss_field = CG<Kernels::Gaussian>(particles, grid, T(0.01));
    Tensor<T> total_gauss_mass{0.0};
    Tensor<T,3> total_gauss_velocity{0.0,0.0,0.0};
    for(size_t i=0; i<grid.size(); i++)
    {
        total_gauss_mass += gauss_field[i].get<0>();   
        total_gauss_velocity += gauss_field[i].get<1>();   
    }
    std::cout<<"Gaussian CG Totals:\t\tmass="<<total_gauss_mass*grid.cell_volume<<"\tvelocity="<<total_gauss_velocity*grid.cell_volume<<"\n";


    //Heaviside Coarse Graining
    auto heavi_field = CG<Kernels::Heaviside>(particles, grid, T(0.01));
    Tensor<T> total_heavi_mass{0.0};
    Tensor<T,3> total_heavi_velocity{0.0,0.0,0.0};
    for(size_t i=0; i<grid.size(); i++)
    {
        total_heavi_mass += heavi_field[i].get<0>();   
        total_heavi_velocity += heavi_field[i].get<1>();   
    }
    std::cout<<"Heaviside CG Totals:\t\tmass="<<total_heavi_mass*grid.cell_volume<<"\tvelocity="<<total_heavi_velocity*grid.cell_volume<<"\n";


    //Gaussian Coarse Graining Gradient
    auto gradient_field = CGGradient<Kernels::Gaussian>(particles, grid, T(0.01));
    Tensor<T,3> total_gradient_mass{0.0,0.0,0.0};
    Tensor<T,3,3> total_gradient_velocity{0.0,0.0,0.0, 0.0,0.0,0.0, 0.0,0.0,0.0};
    for(size_t i=0; i<grid.size(); i++)
    {
        total_gradient_mass += gradient_field[i].get<0>();
        total_gradient_velocity += gradient_field[i].get<1>();   
    }
    std::cout<<"CG Gradient Totals:\t\tmass="<<total_gradient_mass*grid.cell_volume<<"\tvelocity="<<total_gradient_velocity*grid.cell_volume<<"\n";


    return 0;
}
