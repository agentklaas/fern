#pragma once
#include "Grid.h"
#include "Tuple.h"
#include "utils.h"
#include <cmath>
#include <vector>
#include <cstring>


namespace Fern
{

    template<typename T, size_t D, typename PARTICLE>
    constexpr std::vector<decltype(PARTICLE::properties)> Discretize(const std::vector<PARTICLE>& particles, const Grid<T,D>& grid)
    {
        using PROPS_TYPE = decltype(PARTICLE::properties);
        std::vector<PROPS_TYPE> cells(grid.size());
        std::memset(cells.data(), 0, cells.size()*sizeof(PROPS_TYPE));


        T cvol = Utils::sphvol<T> * grid.cell_volume;
        for(size_t i=0; i<particles.size(); i++)
        {
            if(grid.contains(particles[i].position))
            {
                T dvol = 1.0/(particles[i].radius * particles[i].radius * particles[i].radius * cvol);
                T L2 = Utils::ftpioth<T> * particles[i].radius;
                Box<T,D> box{particles[i].position-L2, particles[i].position+L2};

                Tensor<size_t,D> min_cell = grid.bounded_cell_at(box.min);
                Tensor<size_t,D> max_cell = grid.bounded_cell_at(box.max);
                Tensor<size_t,D> cell = min_cell;
                for(bool looping=true; looping; looping=iterate(min_cell, max_cell, cell))
                {
                    T weight = intersection_volume(grid.cell_box(cell), box) * dvol; 
                    Utils::TMAC(cells[grid.index(cell)], particles[i].properties, weight);
                }
            }
        }
        return cells;
    }

    template<template<typename T, size_t D> typename PHI, typename T, size_t D, typename PARTICLE>
    constexpr std::vector<decltype(PARTICLE::properties)> CG(const std::vector<PARTICLE>& particles, const Grid<T,D>& grid, const T& width)
    {
        using PROPS_TYPE = decltype(PARTICLE::properties);
        std::vector<PROPS_TYPE> cells(grid.size());
        std::memset(cells.data(), 0, cells.size()*sizeof(PROPS_TYPE));

        PHI<T,D> phi(grid,width);
        for(size_t i=0; i<particles.size(); i++)
        {
            if(grid.contains(particles[i].position))
            {
                Tensor<size_t,D> min_cell = grid.bounded_cell_at(particles[i].position - phi.cutoff);
                Tensor<size_t,D> max_cell = grid.bounded_cell_at(particles[i].position + phi.cutoff);
                Tensor<size_t,D> cell = min_cell;
                for(bool looping=true; looping; looping=iterate(min_cell, max_cell, cell))
                {
                    Tensor<T,D> r = grid.cell_center(cell) - particles[i].position;
                    T weight = phi(r);
                    Utils::TMAC(cells[grid.index(cell)], particles[i].properties, weight);
                }
            }
        }
        return cells;
    }

    template<template<typename T, size_t D> typename PHI, typename T, size_t D, typename PARTICLE>
    constexpr std::vector<Utils::TupleAddDims<decltype(PARTICLE::properties),D>> CGGradient(const std::vector<PARTICLE>& particles, const Grid<T,D>& grid, const T& width)
    {
        using GRADIENT_TYPE = Utils::TupleAddDims<decltype(PARTICLE::properties), D>;
        std::vector<GRADIENT_TYPE> cells(grid.size());
        std::memset(cells.data(), 0, cells.size()*sizeof(GRADIENT_TYPE));

        PHI<T,D> phi(grid, width);
        for(size_t i=0; i<particles.size(); i++)
        {
            if(grid.contains(particles[i].position))
            {
                Tensor<size_t,D> min_cell = grid.bounded_cell_at(particles[i].position - phi.cutoff);
                Tensor<size_t,D> max_cell = grid.bounded_cell_at(particles[i].position + phi.cutoff);
                Tensor<size_t,D> cell = min_cell;
                for(bool looping=true; looping; looping=iterate(min_cell, max_cell, cell))
                {
                    Tensor<T,D> r = grid.cell_center(cell) - particles[i].position;
                    Tensor<T,D> weight = phi.gradient(r);
                    Utils::TOPAC(cells[grid.index(cell)], particles[i].properties, weight);
                }
            }
        }
        return cells;
    }


}
