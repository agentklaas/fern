#pragma once

#include "Tensor.h"

namespace Fern
{

    template<typename T, size_t D>
    struct Box
    {
        Tensor<T,D> min;       
        Tensor<T,D> max;
    };

    template<size_t N=0, typename T, size_t D>
    constexpr T intersection_volume(const Box<T,D>& lhs, const Box<T,D>& rhs)
    {
        if constexpr(N+1 < D)
        {
            return ((lhs.max[N]<rhs.max[N]?lhs.max[N]:rhs.max[N]) - (lhs.min[N]>rhs.min[N]?lhs.min[N]:rhs.min[N])) * intersection_volume<N+1>(lhs, rhs);
        }
        else
        {
            return (lhs.max[N]<rhs.max[N]?lhs.max[N]:rhs.max[N]) - (lhs.min[N]>rhs.min[N]?lhs.min[N]:rhs.min[N]);
        }
    }
}
