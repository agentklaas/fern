#pragma once
#include "Tuple.h"
#include <cmath>

namespace Fern
{
    namespace Utils 
    {

        template<typename T>
        static constexpr T sqrt2pi = T(std::sqrt(2.0*M_PI));

        template<typename T>
        static constexpr T sphvol = T(4.0/3.0 * M_PI);

        template<typename T>
        static constexpr T ftpioth = T(std::pow( 4.0 * M_PI / 3.0 , 1.0/3.0) * 0.5);



        template<typename, size_t>
        struct TupleAddDimsStruct;

        template<template<typename...> typename TUPLE, typename... T, size_t D>
        struct TupleAddDimsStruct<TUPLE<T...>, D>
        {
            using type = Tuple<typename T::template TypeAddDimsBack<D>...>;
        };

        template<typename TUPLE, size_t D>
        using TupleAddDims = TupleAddDimsStruct<TUPLE, D>::type;



        template<size_t N=0, typename T, typename... PROPS>
        constexpr void TMAC(Tuple<PROPS...>& accumulator, const Tuple<PROPS...>& contributor, const T& factor) //tuple multiply accumulate
        {
            MAC(accumulator.template get<N>(), contributor.template get<N>(), factor);
            if constexpr(N+1 < Tuple<PROPS...>::size())
            {
                TMAC<N+1>(accumulator, contributor, factor);
            }
        }

        template<size_t N=0, typename T, size_t D, typename... PROPS>
        constexpr void TOPAC(TupleAddDims<Tuple<PROPS...>,D>& accumulator, const Tuple<PROPS...>& contributor, const Tensor<T,D>& vector) //tuple outer_product accumulate
        {
            OPAC(accumulator.template get<N>(), contributor.template get<N>(), vector);
            if constexpr(N+1 < Tuple<PROPS...>::size())
            {
                TOPAC<N+1>(accumulator, contributor, vector);
            }
        }


    }

}
